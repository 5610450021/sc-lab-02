package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream.GetField;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class BankAcc_view extends JFrame{
		private static final int FRAME_WIDTH = 450;
		private static final int FRAME_HEIGHT = 100;
		private static final double INITIAL_BALANCE = 1000;
		private JLabel rateLabel;
		private JTextField rateField;
		private JButton button;
		private JLabel resultLabel;
		private JPanel panel;
		
		public BankAcc_view(){
			createFrame();
		}
		
		private void createFrame()
		   {
			final int FIELD_WIDTH = 10;
		    this.rateLabel = new JLabel("Interest Rate: ");
		    this.rateField = new JTextField(FIELD_WIDTH);
		    this.resultLabel = new JLabel("balance: " +INITIAL_BALANCE );
		    this.button = new JButton("Add Interest");
		    this.button = new JButton("Add Interest");
		    this.panel = new JPanel();
		    panel.add(rateLabel);
			panel.add(rateField);
			panel.add(button);
			panel.add(resultLabel);      
			add(panel);
		   }
		public void setResult(String text){
			resultLabel.setText(text);
		}
		public String getText(){
			return rateField.getText();
		}
		public void setListener(ActionListener list) {
			// TODO Auto-generated method stub
			button.addActionListener(list);  
		}
	}