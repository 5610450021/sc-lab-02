package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;

import model.BankAccount;
import view.BankAcc_view;


public class BankAcc_control {
	private static final double DEFAULT_RATE = 5;
	private static final double INITIAL_BALANCE = 1000;
	private static final int FRAME_WIDTH = 450;
	private static final int FRAME_HEIGHT = 100;
	private BankAccount account; 
//	BankAccount is attribute because it have to use in many method. 
//	BankAccount calls attribute in class controller. 
	private ActionListener list;
	private BankAcc_view frame;
	 
	   class AddInterestListener implements ActionListener{
		   
		   public void actionPerformed(ActionEvent event){
			   double rate = Double.parseDouble(frame.getText());
			   double interest = account.getBalance() * rate / 100;
			   account.deposit(interest);
			   frame.setResult("balance: " + account.getBalance());
        }       
	   }

        public static void main(String[] args) {
    		// TODO Auto-generated method stub
    		new BankAcc_control();
    	}
	   
	   public BankAcc_control(){
		   frame = new BankAcc_view();
		   frame.pack();
		   frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		   list = new AddInterestListener();
		   frame.setListener(list);
		   account = new BankAccount(INITIAL_BALANCE);
		   frame.setVisible(true);
	   }
	        
}

